module warmfusion/goboom

go 1.12

require (
	contrib.go.opencensus.io/exporter/prometheus v0.1.0
	github.com/apex/log v1.1.1
	github.com/prometheus/client_golang v1.3.0
	go.opencensus.io v0.22.2
)
