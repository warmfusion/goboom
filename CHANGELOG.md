# GoBoom

## 1.1.0 - 2020-03-07

- New Plugin - Continuity adding information on;
    - Time to Interactive
    - Visual Ready
    - First Input Delay


## 1.0.1 - 2020-02-24
- Bugfix - TTFB not calculated properly; used END of response and not START for its calculation
- Added Changelog

## 1.0.0

- Initial release with Prometheus exporter data
    - TTFB, SSL, DNS resolution times
    - OpenCensus data for service itself