package exporters

import (
	"strconv"
	"warmfusion/goboom"

	"github.com/prometheus/client_golang/prometheus"
	"github.com/prometheus/client_golang/prometheus/promauto"
)

var pageBucket = []float64{1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 16, 18, 20}

var (
	performanceTimingTTI = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "boomerang_performance_timing_tti",
		Help:    "Time to interactive (TTI) in seconds",
		Buckets: pageBucket,
	}, []string{"hostname"})
	performanceTimingVR = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "boomerang_performance_timing_visually_ready",
		Help:    "Visually ready (VR) in seconds -  it's the earliest possible timestamp in the page's lifecycle that TTI could happen",
		Buckets: pageBucket,
	},
		[]string{"hostname"})
	performanceTimingFID = promauto.NewHistogramVec(prometheus.HistogramOpts{
		Name:    "boomerang_performance_timing_fid",
		Help:    "First input delay (FID) in seconds",
		Buckets: pageBucket,
	},
		[]string{"hostname"})
)

type continuityMetric struct {
	TTI float64
	VR  float64
	FID float64
}

// ContinuityExporter - Creates prometheus data for https://akamai.github.io/boomerang/BOOMR.plugins.Continuity.html
type ContinuityExporter struct {
}

// GetName - Return a friendly name of the exporter for logging
func (ce ContinuityExporter) GetName() string {
	return "Continuity"
}

// ExportBeaconMetrics - Process the incoming beacon into a prometheus event
func (ce ContinuityExporter) ExportBeaconMetrics(b goboom.Beacon, domain string) error {

	// Use the metrics data to generate a set of values
	// we care about
	bmpt, err := createContinuityMetricFromBeacon(b)

	if err != nil {
		return err
	}

	// Now add all the relevant information to prometheus
	if bmpt.TTI > 0 {
		performanceTimingTTI.WithLabelValues(domain).Observe(bmpt.TTI)
	}

	if bmpt.VR > 0 {
		performanceTimingVR.WithLabelValues(domain).Observe(bmpt.VR)
	}

	if bmpt.FID > 0 {
		performanceTimingFID.WithLabelValues(domain).Observe(bmpt.FID)
	}

	return nil
}

func createContinuityMetricFromBeacon(b goboom.Beacon) (continuityMetric, error) {
	bmpt := continuityMetric{}

	// All of these stats are optional, so we should make sure they exist before doing things with them

	// Parse out metrics into useful struct
	tti, exists := b.Metrics["c.tti"]
	if exists {
		ttii, _ := strconv.Atoi(tti)
		bmpt.TTI = float64(ttii / 1000)
	}

	fid, exists := b.Metrics["c.fid"]
	if exists {
		fidi, _ := strconv.Atoi(fid)
		bmpt.FID = float64(fidi / 1000)
	}

	vr, exists := b.Metrics["c.tti.vr"]
	if exists {
		vri, _ := strconv.Atoi(vr)
		bmpt.VR = float64(vri / 1000)
	}

	return bmpt, nil
}
