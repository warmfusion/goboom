package exporters

import (
	"reflect"
	"testing"
	"warmfusion/goboom"
)

func Test_createContinuityMetricFromBeacon(t *testing.T) {
	type args struct {
		b goboom.Beacon
	}
	tests := []struct {
		name    string
		args    args
		want    continuityMetric
		wantErr bool
	}{
		{
			name: "Simple",
			args: args{
				b: goboom.Beacon{
					Metrics: map[string]string{
						"c.tti":    "1000",
						"c.tti.vr": "1000",
						"c.fid":    "1000",
					},
				},
			},
			wantErr: false,
			want: continuityMetric{
				TTI: 1,
				VR:  1,
				FID: 1,
			},
		},
		{
			name: "Empty",
			args: args{
				b: goboom.Beacon{
					Metrics: map[string]string{},
				},
			},
			wantErr: false,
			want:    continuityMetric{},
		},
		{
			name: "One missing",
			args: args{
				b: goboom.Beacon{
					Metrics: map[string]string{
						"c.tti.vr": "1000",
					},
				},
			},
			wantErr: false,
			want: continuityMetric{
				VR: 1,
			},
		},
		{
			name: "One small",
			args: args{
				b: goboom.Beacon{
					Metrics: map[string]string{
						"c.tti.vr": "50",
					},
				},
			},
			wantErr: false,
			want:    continuityMetric{},
		},
		{
			name: "One not so small",
			args: args{
				b: goboom.Beacon{
					Metrics: map[string]string{
						"c.tti.vr": "3999",
					},
				},
			},
			wantErr: false,
			want: continuityMetric{
				VR: 3,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			got, err := createContinuityMetricFromBeacon(tt.args.b)
			if (err != nil) != tt.wantErr {
				t.Errorf("createContinuityMetricFromBeacon() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("createContinuityMetricFromBeacon() = %v, want %v", got, tt.want)
			}
		})
	}
}

// TestContinuityExporter_GetName seems pointless
func TestContinuityExporter_GetName(t *testing.T) {
	tests := []struct {
		name string
		ce   ContinuityExporter
		want string
	}{
		{
			name: "a test",
			ce:   ContinuityExporter{},
			want: "Continuity",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ce := ContinuityExporter{}
			if got := ce.GetName(); got != tt.want {
				t.Errorf("ContinuityExporter.GetName() = %v, want %v", got, tt.want)
			}
		})
	}
}

//TestContinuityExporter_ExportBeaconMetrics seems pointless
func TestContinuityExporter_ExportBeaconMetrics(t *testing.T) {
	type args struct {
		b      goboom.Beacon
		domain string
	}
	tests := []struct {
		name    string
		ce      ContinuityExporter
		args    args
		wantErr bool
	}{
		{
			name: "Simple",
			ce:   ContinuityExporter{},
			args: args{
				b:      goboom.Beacon{},
				domain: "Doesn't mattter",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			ce := ContinuityExporter{}
			if err := ce.ExportBeaconMetrics(tt.args.b, tt.args.domain); (err != nil) != tt.wantErr {
				t.Errorf("ContinuityExporter.ExportBeaconMetrics() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
