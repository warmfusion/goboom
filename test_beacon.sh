#!/bin/sh

ENDPOINT=${1:-127.0.0.1:3000/beacon}
echo "Submitting test beacon to $ENDPOINT"

DATA_PAGE='nt_nav_st=1579963988441&nt_fet_st=1579963988447&nt_dns_st=1579963988454&nt_dns_end=1579963988467&nt_con_st=1579963988467&nt_con_end=1579963988501&nt_req_st=1579963988502&nt_res_st=1579963988591&nt_res_end=1579963988606&nt_domloading=1579963988610&nt_domint=1579963988979&nt_domcontloaded_st=1579963989072&nt_domcontloaded_end=1579963989079&nt_domcomp=1579963990228&nt_load_st=1579963990228&nt_load_end=1579963990280&nt_unload_st=1579963988599&nt_unload_end=1579963988601&nt_ssl_st=1579963988467&nt_enc_size=48232&nt_dec_size=236141&nt_trn_size=49169&nt_protocol=http%2F1.1&nt_spdy=0&nt_cinf=http%2F1.1&nt_first_paint=1579963988799&nt_red_cnt=0&nt_nav_type=1&u=https%3A%2F%2Fwww.techradar.com%2F&v=1.0.0&sm=i&rt.si=e35d9a07-6e79-4f16-841b-54e3d3b432fd-NaN&rt.ss=&rt.sl=0&vis.st=visible&ua.plt=MacIntel&ua.vnd=Google%20Inc.&pid=fuxkj2c2&n=1&sb=1'
DATA_SEARCH='nt_nav_st=1579968780152&nt_fet_st=1579968780156&nt_dns_st=1579968780164&nt_dns_end=1579968780164&nt_con_st=1579968780164&nt_con_end=1579968780185&nt_req_st=1579968780185&nt_res_st=1579968780815&nt_res_end=1579968780827&nt_domloading=1579968780834&nt_domint=1579968780992&nt_domcontloaded_st=1579968781021&nt_domcontloaded_end=1579968781027&nt_domcomp=1579968781945&nt_load_st=1579968781945&nt_load_end=1579968781969&nt_unload_st=1579968780825&nt_unload_end=1579968780826&nt_ssl_st=1579968780164&nt_enc_size=42641&nt_dec_size=179649&nt_trn_size=43541&nt_protocol=http%2F1.1&nt_spdy=0&nt_cinf=http%2F1.1&nt_first_paint=1579968780940&nt_red_cnt=0&nt_nav_type=0&u=https%3A%2F%2Fwww.techradar.com%2Fsearch%3FsearchTerm%3Doh%2Bhi%2Bmy%2Bname%2Bis%2Bme&v=1.0.0&sm=i&rt.si=2a7c931c-3b55-425d-8de7-3bde5a5068c0-NaN&rt.ss=&rt.sl=0&vis.st=visible&ua.plt=MacIntel&ua.vnd=Google%20Inc.&pid=id1s2gik&n=1&sb=1'

curl --request POST \
  --url http://$ENDPOINT \
  --header 'content-type: application/x-www-form-urlencoded' \
  --header 'origin:  http://local.example.com:3000' \
  --header 'referer:  http://local.example.com:3000' \
  --header 'user-agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.79 Safari/537.36' \
  --data ${DATA_SEARCH}


  curl localhost:3000/metrics